require.config({
    baseUrl: "./js/libs/",
    paths: {
        plugins: '../plugins',
        app: '../app',
        views: '../views',
        implementations: '../implementations'
    },
    shim: {
        'jquery.class' : ['jquery'],
        'jquery.pubsub' : ['jquery'],
        'owl.carousel' : ['jquery']
    }
});

require(['app'], function(App) {
    console.log('main / new App()');

    App.init();

});