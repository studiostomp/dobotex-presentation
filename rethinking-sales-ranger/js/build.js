({
    baseUrl: "./libs/",
    paths: {
        plugins: '../plugins',
        app: '../app',
        views: '../views',
        implementations: '../implementations'
    },
    shim: {
        'jquery.class' : ['jquery'],
        'jquery.pubsub' : ['jquery']
    },

    name: "../main",
    out: "built/main.min.js",
    preserveLicenseComments: false
})