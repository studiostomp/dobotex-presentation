define([
	'jquery',
	'views/view',
	'owl.carousel',
	'views/widget'
], function ($, View, owlCarousel) {

	'use strict';

	/**
	 * sub view used for all views inside a section
	 * @type View
	 */
	return View.create('Widget', 'Carousel', {

		setup: function () {

			this._super();

		},

		init: function(el, options) {
			console.log('carousel init');
			this._super(el, options);

			var images = $('body').find('img'),
				i,
				l = images.length,
				image;

			var onload = function(e){
				var image = images[this.id];
				$(image).parent().removeClass('queued');
				$(image)[0].src = this.src;
			};

			for(i = 0; i < l; i++){
				image = new Image();
				image.id = i;
				image.onload = onload;
				image.src = $(images[i]).data('source');
			}

			this.carouselSetup();

			this.setHash();

			this.enable();
		},

		enable: function() {

			this._super();
			$(document).on('keyup.' + this.id, $.proxy(this, 'onKeyUp'));
			$('.sidebar li').on('click.' + this.id, $.proxy(this, 'onSidebarClick'));

			setTimeout($.proxy(this, 'hideSidebar'), 1500);
		},

		disable: function () {

			this._super();
			this.$el.off('.' + this.id);

		},

		hideSidebar:function(e){
			$('.sidebar').removeClass('init');
		},

		onContextMenu: function(e){
			return false;
		},

		setHash: function(){
			var hash;

			if(location.hash === '') {
				location.hash = '0';
			} else {
				hash = location.hash.replace('#', '');
				this.goToSlide(hash);
			}

		},

		onSidebarClick: function(e){
			e.preventDefault();
			var index = $(e.currentTarget).index();
			this.goToSlide(index);
		},

		goToSlide: function(slide) {
			console.log('goTo => ', slide);
			window.owl.goTo(slide);
		},

		carouselSetup: function() {

			var self = this;
			console.log(this.$el);

			this.owl = this.$el.owlCarousel({
				singleItem:true,
				slideSpeed: 700,
				afterMove: $.proxy(self, 'onAfterMove')
			});

			window.owl = this.owl.data('owlCarousel');

			console.log('owl => ', this.owl);

		},

		onAfterMove: function(){
			var index = window.owl.currentItem;
			location.hash = index;

			$('.sidebar li').removeClass('active');
			var item = $('.sidebar').find('li')[index];
			$(item).addClass('active');

			var h = $('.sidebar nav li').first().height() + 30;

			$('.sidebar nav').animate({
				scrollTop: (h * index) - ($(window).height() / 1.65)
			}, 500);
		},

		onKeyUp: function(e) {
			if(e.keyCode == 37) {
				window.owl.prev();
			} else if(e.keyCode == 39) {
				window.owl.next();
			}
		}

	});

});