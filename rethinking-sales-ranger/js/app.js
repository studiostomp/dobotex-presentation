define(['jquery',
	'jquery.class',
	'views/widget.carousel'
	],
	function($) {

	var App = {

		init: function () {

			// set elements that will be reused
			this.$body = $('body');

			// init js view modules
			this.$body.initViews();

		}

	};

	return App;

});