<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dobotex - Rethinking Sales Ranger</title>
        <meta name="description" content="Presentation Dobotex - Rethinking Sales Ranger">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="apple-mobile-web-app-title" content="Rethinking Sales Ranger">

        <link rel="shortcut icon" href="/images/favicon.ico">
        <link rel="apple-touch-icon" href="/images/apple-touch-icon.png">
        <link rel="apple-touch-icon-precomposed" href="/images/apple-touch-icon-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/images/apple-touch-icon-57x57-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/images/apple-touch-icon-72x72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/images/apple-touch-icon-114x114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/images/apple-touch-icon-144x144-precomposed.png">

        <link rel="stylesheet" href="css/style.css">
    </head>


    <body oncontextmenu="return false;">
        <!-- Add your site or application content here -->
        <ul class="owl-carousel" data-view="Widget.Carousel">
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/00.1-splash.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/00.1-legend.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/00.2-intro.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/00.3-context.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/00.4-architecture.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/00.5-design.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/00.6-windows-homescreen.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/00.7-splash-levis.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/01-browse-displays.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/01.1-browse-displays-app-bar.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/01.2-browse-displays-small-tiles.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/01.3-browse-displays-filter-panel.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/01.4-browse-displays-filter-compact.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/02.0-new-display-with-options.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/02.1-new-display-clean.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/03.1-browse-collection.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/03.2-browse-collection-app-bar.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/03.3-browse-collection-app-bar-select-mode.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/03.4-browse-collection-filter-panel.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/04.1-product-detail.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/04.1.1-product-detail-full-screen-image.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/04.1.2-product-detail-full-screen-image.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/04.1.3-product-detail-back.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/05.1-selection-panel.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/05.2.1-selection-panel-remove.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/05.2.2-selection-panel-remove.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/05.3-selection-panel-product-detail.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/05.4-selection-highlight.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/05.5-selection-drag.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/05.6-selection-drag-ready.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/05.7-selection-drag-full-front.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/06-statistics.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/07-branded-ui-examples.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/07.1-levis-branded-ui.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/07.2-levis-branded-ui.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/07.3-tommy-branded-ui.png"></li>
            <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/10.splash-dobotex.png"></li>
        </ul>

        <div class="sidebar init">
        <nav>
            <ul>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/00.1-splash.png"><span>...</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/00.1-legend.png"><span>1</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/00.2-intro.png"><span>2</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/00.3-context.png"><span>3</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/00.4-architecture.png"><span>4</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/00.5-design.png"><span>5</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/00.6-windows-homescreen.png"><span>6</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/00.7-splash-levis.png"><span>7</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/01-browse-displays.png"><span>8</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/01.1-browse-displays-app-bar.png"><span>9</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/01.2-browse-displays-small-tiles.png"><span>10</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/01.3-browse-displays-filter-panel.png"><span>11</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/01.4-browse-displays-filter-compact.png"><span>12</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/02.0-new-display-with-options.png"><span>13</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/02.1-new-display-clean.png"><span>14</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/03.1-browse-collection.png"><span>15</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/03.2-browse-collection-app-bar.png"><span>16</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/03.3-browse-collection-app-bar-select-mode.png"><span>17</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/03.4-browse-collection-filter-panel.png"><span>18</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/04.1-product-detail.png"><span>19</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/04.1.1-product-detail-full-screen-image.png"><span>20</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/04.1.2-product-detail-full-screen-image.png"><span>21</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/04.1.3-product-detail-back.png"><span>22</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/05.1-selection-panel.png"><span>23</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/05.2.1-selection-panel-remove.png"><span>24</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/05.2.2-selection-panel-remove.png"><span>25</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/05.3-selection-panel-product-detail.png"><span>26</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/05.4-selection-highlight.png"><span>27</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/05.5-selection-drag.png"><span>28</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/05.6-selection-drag-ready.png"><span>29</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/05.7-selection-drag-full-front.png"><span>30</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/06-statistics.png"><span>31</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/07-branded-ui-examples.png"><span>32</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/07.1-levis-branded-ui.png"><span>33</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/07.2-levis-branded-ui.png"><span>34</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/07.3-tommy-branded-ui.png"><span>35</span></li>
                <li class="queued" ><img src="images/ajax-loader.gif" data-source="media/10.splash-dobotex.png"><span>36</span></li>
            <ul>
        </nav>
    </div>

        <!-- Javascript imports -->
        <script>
            var DEBUG = false;
            if(!DEBUG){
            if(!window.console) window.console = {};
            var methods = ["log", "debug", "warn", "info"];
            for(var i = 0; i < methods.length; i++){
               console[methods[i]] = function(){};
            }
            }
        </script>
       <!--script data-main="js/main" src="js/plugins/require.js"></script-->
       <script data-main="js/built/main.min" src="js/plugins/require.js"></script>
    </body>
</html>